codeunit 70200 "TCN_SuscripcionesExtPre"
{
    SingleInstance = true;

    [EventSubscriber(ObjectType::Table, Database::TCN_HistoricoPresencia, 'OnAfterCalcularDifHoras', '', false, false)]
    local procedure OnAfterCalcularDifHorasTableTCNHistoricoPresenciaF(var prRec: Record TCN_HistoricoPresencia)
    var
        rlTCNTurno: Record TCN_Turno;
        // Se crea para poder acceder al campo minutos para poder restárselo al número de horas
        rlTCNHistoricoPresencia: Record TCN_HistoricoPresencia;
        // Lo creamos exclusivamente para contar el número de registros que hay
    begin
        // rlTCNTurno.find(prRec.CodTurno);      
        if (prRec.NumeroHoras <> 0) and (prRec.TipoMovimiento = prRec.TipoMovimiento::Presencia) then begin
            // Filtramos que el número de horas sea distinto de 0, ya que sino no se estarían computando horas
            // Filtramos por tipo de moviviento, ya que si es un ausencia, no tendremos que descontar los minutos de descanso
            rlTCNHistoricoPresencia.SetRange(NumEmpleado, prRec.NumEmpleado);
            rlTCNHistoricoPresencia.SetRange(FechaInicio, prRec.FechaInicio);
            rlTCNHistoricoPresencia.SetRange(TipoMovimiento, prRec.TipoMovimiento::Presencia);
            // Se crea otro filtro según el tipo de movimiento porque estamos "moldeando" el registro, sino nos traería todos los registros de ausencia y de presencia
            if rlTCNHistoricoPresencia.Count() = 1 then begin
                rlTCNTurno.Get(prRec.CodTurno);
                // No le pongo un if, porque como es un boolean, devolvería un error en caso de no encontrar un registro
                // En el momento en el que hago el get, en función de su código, en mi objeto rlTCNHistoricoPresencia, ya tendría un registro guardado de la base de datos SQL                
                prRec.Validate(NumeroHoras, Round(prRec.NumeroHoras - (rlTCNTurno.MinDesayunoDescanso) / 60, 0.01));
            end;
        end;
    end;
}